import sys

# TODO: Akzeptiert auch hostnamen, prüfen, ob IP/Host zum eigenen System gehört
# UDP_IP:
# -------
# IP des Netzwerk-Devices, auf dem nach Now&Next-Paketen gelauscht werden soll.
# Nur lokal lauschen (nur zum testen sinnvoll)
# UDP_IP = "127.0.0.1"
#
# Auf allen Netzwerk-Geräten lauschen
# UDP_IP = "0.0.0.0"
#
# Vorgabe: IP des Netzwerk-Devices, was die Verbindung zwischen Studio- und
# Streamrechner erstellt.
#
# UDP_IP = "10.11.12.13"

UDP_IP = "0.0.0.0"

# UDP_PORT:
# ---------
# Port auf dem die Nachrichten via UDP eingehen. Den Port nachsehen in Rivendell
# unter: rdadmin -> Manage Hosts -> Studio-Rechner -> RDAirplay -> Now&Next Data
#
# Vorgabe: 5000

UDP_PORT = 5000

# OUT_UDP_PORT:
# OUT_UDP_IP:
# ---------
# Port und IP auf dem die Nachrichten via UDP weitergeletet werden.
# So kann ein Backup-Streamrechner parallel laufen und die Daten
# mitschneiden.
#
# Vorgabe: 5001, 127.0.0.1

OUT_UDP_PORT = 5001
OUT_UDP_IP = "127.0.0.1"

# MANUAL MODE
# -----------
# When active, ignore all data except its group matches MANUAL_GROUP
# MANUAL_START_STRING and MANUAL_STOP_STRING can be send to the program the same way as metadata
# to (de)activate manual mode.
MANUAL_GROUP = 'MANUAL'
MANUAL_START_STRING = '%ENTERMANUAL%'
MANUAL_STOP_STRING = '%EXITMANUAL%'

# WANTED_GROUPS:
# --------------
# Welche Gruppen sollen in die Plylist eingebunden werden? Namen in Rivendell
# nachsehen in rdadmin -> Manage Groups
# Groß- und Kleinschreibung beachten.

WANTED_GROUPS = ["JINGLES", "MUSIK", "MUSIK_ALT", "WORT", "TRAILER", "STATION_ID", "AUTO_TT", "AUTO", "TEASER",
                 "ZEIT", MANUAL_GROUP]

# TODO: Pfade via Variable
# HIRSE_HOME="/home/ices/"
HIRSE_HOME = ""

PID_FILE_DIR = HIRSE_HOME + "run/"
PID_FILE = PID_FILE_DIR + "hertz-metadata-listener.pid"
TMP_DIR = HIRSE_HOME + "tmp/"
XSPF_FRAGMENT_FILENAME = TMP_DIR + 'xspf-current-fragment'
PLAIN_FRAGMENT_FILENAME = TMP_DIR + 'plain-current-fragment'
CURRENT_ARTIST_FILENAME = TMP_DIR + 'current.artist'
CURRENT_TITLE_FILENAME = TMP_DIR + 'current.title'
HIRSE_META_SCRIPT_DIR = HIRSE_HOME + 'etc/meta.d'

# Timezone for pytz. In an ideal world should be generated automatically.
LOCAL_TIMEZONE = "Europe/Berlin"

LOG_TARGET = sys.stdout
LOG_FORMAT = "[%(levelname)s] %(message)s"

RECURRING_ERROR = False

# SPLIT_CHAR:
# -----------
# Zeichen, welches die einzelnen Felder trennt. Gnaues Format den
# Now&Next-Einstellungen entnehmen. Derzeit:
#
# "%g|%a|%t|%h %r"
#
# %g = group, %a = artist, %t = track, %h = dauer in ms, %r = Zeilenumbruch

SPLIT_CHAR = "|"

# XSPF_META_TIMESTAMP_STRING:
# ---------------------------
# Zeichenkette, für die Metadaten in der Playliste. Arg unwichtig.
XSPF_META_TIMESTAMP_STRING = "http://stream.radiohertz.de/xspf/timestamp"

# Datei schreiben. Nimmt vorbereiten String, Pfad und Dateiname entgegen.
# Die Ausgabe-Datei wird überschrieben.
