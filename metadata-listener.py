#!/usr/bin/python3
# -*- coding: utf8 -*-
import socket
import re
import errno
import os
import logging
import pytz  # required package: python-tz
from xml.sax.saxutils import escape
import subprocess as sp

from datetime import datetime
from optparse import OptionParser

import unicodedata

from config import *

# TODO: Von .hirse.rc Variablen lesen

# http://stackoverflow.com/questions/3503719/emulating-bash-source-in-python
# command = ['bash', '-c', 'source .hirse.rc && env']
# proc = subprocess.Popen(command, stdout = subprocess.PIPE)

# TODO: Haverie-System / Restart Listener (while true??) -> lieber via bash?

# Dieses Skript lauscht auf Now-and-Next Nachrichten von Rivendell, die
# Angaben über das gerade gespielte Event (Song, Jingle, Beitrag, …) enthalten.
# Rivendell sendet per UDP. Daher Port und IP vom lauschenden Rechner angeben.

# Das Skript läuft non-stop bis es per CTRL-C o.ä. beendet wird oder auf einen
# Netzwerk-Fehler stößt.

# Exit Codes:
# 
#   0   Beendet mit CTRL-C.
#
#   1   Generischer Fehler. 
#
#   5   Gewählte IP:Port-Kombination ist bereits belegt.


# TODO: Exceptions permission, etc
# filename must contain dirpath! (but makes that sense?)
def write_file(string, dirname, filename):
    try:
        if not os.path.isdir(dirname):
            os.makedirs(dirname)
        # TODO: Warum nicht dirname mitgeben? 
        outputfile = open(filename, 'w')
        outdata = string
        # outdata = string.encode("utf8")
        outputfile.write(outdata)
        outputfile.close()
        # logging.debug("wrote " + string + " in " + filename + " placed at " + dirname)
    except IOError:
        logging.exception("Error handling file.")
        clean_exit(1)


# TODO: Fehlermeldungen/Debug optional in Logdatei
# http://stackoverflow.com/questions/6579496/using-print-statements-only-to-debug
# https://docs.python.org/2/howto/logging.html

# TODO: Output korrekt an log-datei (via stdout/stderr)

# Deletes Unicode control characters
# http://stackoverflow.com/a/19016117 
def remove_control_characters(s):
    return "".join(ch for ch in s if unicodedata.category(ch)[0] != "C")


def pid_file_write():
    pid = str(os.getpid())
    logging.debug("My PID is: " + pid)
    logging.debug("My PID-Dir is: " + PID_FILE_DIR)
    logging.debug("My PID-File is: " + PID_FILE)
    write_file(pid, PID_FILE_DIR, PID_FILE)


def pid_file_delete():
    # TODO: Exception handling
    pid_fullpath = PID_FILE_DIR + PID_FILE
    if os.path.isfile(pid_fullpath):
        os.remove(pid_fullpath)


def clean_exit(code):
    pid_file_delete()
    sys.exit(code)


def parse_args():
    # Optionen zuweisen, die später ausgewertet werden sollen/können.
    # TODO: use argparser instead?

    parser = OptionParser(version="%prog 0.10",
                          usage="usage: %prog [options]")

    # Vorgabewerte für Optionen
    parser.set_defaults(verbose=False)
    parser.set_defaults(web=False)
    parser.set_defaults(text=False)
    parser.set_defaults(xspf=False)
    parser.set_defaults(dev=False)
    parser.set_defaults(manual=False)
    parser.set_defaults(port=UDP_PORT)
    parser.set_defaults(ip=UDP_IP)
    parser.set_defaults(rdnownext=False)
    parser.set_defaults(outport=OUT_UDP_PORT)
    parser.set_defaults(outip=OUT_UDP_IP)

    parser.add_option("-p", "--port", dest="port", type="int", help="Port to listen")
    parser.add_option("-i", "--ip", dest="ip", help="IP (v4) to listen")
    parser.add_option("-w", "--web", action="store_true",
                      help="Save output for the webserver")
    parser.add_option("-t", "--text", action="store_true",
                      help="Save output in plain-text")
    parser.add_option("-x", "--xspf", action="store_true",
                      help="Save output in XSPF")
    parser.add_option("-d", "--dev", action="store_true",
                      help="Activate verbose mode and don't run follow-up commands")
    parser.add_option("-m", "--manual", action="store_true",
                      help="Start in manual mode (don't accept data from rivendell)")
    parser.add_option("-v", "--verbose", action="store_true",
                      help="Enable verbose output")
    parser.add_option("-r", "--rdnownext", action="store_true",
                      help="Simulate Rivendell Now+Next behavior. Forward received Now+Next data via UDP.")
    parser.add_option("-o", "--outport", dest="outport", type="int", help="Port to forward Now+Next data.")
    parser.add_option("-u", "--outip", dest="outip", help="IP to forward Now+Next data.")

    # Option-Parser starten und Kommandozeilen-Optionen auslesen
    (option, args) = parser.parse_args()

    # activate verbose output in any case if in dev mode
    if option.dev:
        option.verbose = True

    if option.verbose:
        logging.basicConfig(stream=LOG_TARGET, format=LOG_FORMAT, level=logging.DEBUG, force=True)

    # Hilfsvariable für Statusmeldungen
    status = ["Off", "On"]

    # Ausgabe, welche Output-Formate erzeugt werden.
    logging.debug("web:  " + status[int(option.web)])
    logging.debug("text: " + status[int(option.text)])
    logging.debug("xspf: " + status[int(option.xspf)])
    logging.debug("rdnn: " + status[int(option.rdnownext)])
    logging.debug("dev: " + status[int(option.dev)])
    logging.debug("manual: " + status[int(option.manual)])

    if not option.web and not option.text and not option.xspf:  # Kein Output aktiviert
        parser.error('No output selected.\n\t\t\t     Use at least one of -t, -w and/or -x')

    return option


# Removing ms from Isoformat. Expect string from datetime.isoformat()
def get_clean_xmltime(datestr):
    return re.sub(r'\..*?\+', '+', datestr)


# XML von Hand bauen. Für das bissl XSPF lohnt sich keine lib.
# Es gibt xspf.py, aber das hatte <meta> noch nicht implementiert.
# Create <track>-element for XSPF. This is only a fragment, not valid XSPF
def create_xspf_track(artist, song, ms, utc_date):
    # Generate pytz-timezone object for conversion
    date = utc_date.astimezone(pytz.timezone(LOCAL_TIMEZONE))
    date_str = get_clean_xmltime(date.isoformat())

    try:
        xmltitle = "\t<title>" + escape(song) + "</title>\n"
        xmlcreator = "\t<creator>" + escape(artist) + "</creator>\n"
        xmlduration = "\t<duration>" + ms + "</duration>\n"
        xmlmeta = "\t<meta rel=\"" + XSPF_META_TIMESTAMP_STRING + "\">" + date_str + "</meta>\n"
    except Exception:
        logging.exception("Create XSPF Error: ")
        xmltitle = "\t<title>Unknown Song</title>\n"
        xmlcreator = "\t<creator>Unknown Artist</creator>\n"
        xmlduration = "\t<duration>0</duration>\n"
        xmlmeta = "\t<meta rel=\"" + XSPF_META_TIMESTAMP_STRING + "\">" + date_str + "</meta>\n"

    xmltrack = "<track>\n" + xmltitle + xmlcreator + xmlduration + xmlmeta + "</track>\n"
    logging.debug("----------------\nXSPF output:\n" + xmltrack + "----------------")
    write_file(xmltrack, TMP_DIR, XSPF_FRAGMENT_FILENAME)


# Create plain-text key/value output. Depends on the options at starttime
# it creates the full file (like vorbis-format) and/or webserver files.
def create_plain_output(artist, song, group, ms, utc_date, text: bool, web: bool):
    date = utc_date.astimezone(pytz.timezone(LOCAL_TIMEZONE))
    creator = "artist=" + artist + "\n"
    title = "title=" + song + "\n"
    genre = "genre=" + group + "\n"
    # Hier wollen wir nur die Sekunden.
    duration = "playTime=" + repr(int(round(int(ms) / 1000.))) + "\n"
    year = "startYear=" + repr(date.year) + "\n"
    month = "startMonth=" + repr(date.month) + "\n"
    day = "startDay=" + repr(date.day) + "\n"
    time = "startTime=" + date.strftime("%H:%M:%S") + "\n"
    comment = 'comment=\n'

    track = creator + title + genre + duration + year + month + day + time + comment

    logging.debug("----------------\nPlain output:" + track + "----------------")

    if text:
        logging.debug("Creating plain output")
        write_file(track, TMP_DIR, PLAIN_FRAGMENT_FILENAME)
    if web:
        logging.debug("Creating output for webserver")
        write_file(artist + "\n", TMP_DIR, CURRENT_ARTIST_FILENAME)
        write_file(song + "\n", TMP_DIR, CURRENT_TITLE_FILENAME)


# Forward received data to another computer

def forward_nownext_data(data):
    outsock = socket.socket(socket.AF_INET,  # Internet
                            socket.SOCK_DGRAM)  # UDP
    # if(option.unicode):
    outsock.sendto(data, (OUT_UDP_IP, OUT_UDP_PORT))
    # else:
    #    sock.sendto(data.decode("utf-8").encode("iso-8859-15"), (OUT_UDP_IP, OUT_UDP_PORT))


def main():

    logging.basicConfig(stream=LOG_TARGET, format=LOG_FORMAT, level=logging.INFO)
    option = parse_args()

    # Lege PID-file an
    pid_file_write()

    # set manual mode or not
    manual_mode = option.manual

    # Werte aus den Kommandozeilen-Optionen für das Skript übernehmen
    udp_ip = option.ip
    udp_port = option.port

    # sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # sock.bind((udp_ip, udp_port))

    # Diese Zeichenkette udp_string dient nur für eine verständlichen Fehlermeldung.
    udp_string = udp_ip + ':' + repr(udp_port)
    out_udp_string = OUT_UDP_IP + ':' + repr(OUT_UDP_PORT)

    # Hier lauscht das Skript konkret auf der angegebenen IP/PORT
    # TODO: IPv6 ?? -> Kann Rivendell nicht.

    try:
        sock = socket.socket(socket.AF_INET,  # Internet
                             socket.SOCK_DGRAM)  # UDP
        sock.bind((udp_ip, udp_port))
    except socket.error as e:
        sock_err_str = "[" + udp_string + "]: "
        errorcode = e.errno
        errordesc = e.strerror
        # Fehler, die nicht zu einem exit führen sollen, sind hier nicht vorgesehen
        # Sonst sys.exit in die entsprechenden if-teile reinverschieben.
        if errorcode == errno.EADDRINUSE:
            logging.error(sock_err_str + errordesc)
            # Spezieller Exitcode für die Bash, damit wir in dem Fall warten können.
            clean_exit(5)
        elif errorcode == socket.EAI_NODATA or errno.EACCES:
            logging.error(sock_err_str + errordesc)
        elif errorcode == socket.EAI_ADDRFAMILY:
            logging.error(sock_err_str + errordesc + ". No IPv6 support, yet.")
        else:
            logging.error(sock_err_str + " Error " + repr(errorcode) + ", " + errordesc)
        clean_exit(1)
    except OverflowError as e:
        # Wir wollen nur die Beschreibung "port must be 0-65535" aus
        # OverflowError('getsockaddrarg: port must be 0-65535.',)
        errordesc = repr(e).split("'")[1].split(":")[1]
        logging.error("[" + udp_string + "]: " + errordesc)
        clean_exit(1)
    except Exception:
        logging.exception("Unknown Error: ")
        clean_exit(1)

    # Variablen die zur Auswertung benötigt werden
    artist = ""
    song = ""
    group = ""
    ms = ""

    former_artist = ""
    former_song = ""

    # Dauerschleife, für jedes eingehende Paket wird sie einmal durchlaufen
    while True:
        try:
            logging.debug('Listening on ' + udp_string)
            if option.rdnownext:
                logging.debug('Forwarding to ' + out_udp_string)

            # for my IDE to shut up, because we exit if it's undefined:
            # noinspection PyUnboundLocalVariable
            incoming, addr = sock.recvfrom(1024)  # buffer size is 1024 bytes
            # TODO: Nur pakte vom studiorechner annehmen
            # pseudocode: if addr=studio-pc-ip, then data = incoming, else drop

            # We expect ISO-8859-15 from Rivendell, we ignore everything outside
            data = incoming.decode('iso-8859-15', 'ignore')
            logging.debug('Received message from ' + repr(addr) + ': ' + repr(data))

            # Empfangenes Packet unbehandelt weiterleiten
            if option.rdnownext:
                forward_nownext_data(incoming)

            # (De)Aktivieren des manuellen Modus (Rivendell-Pakete werden ignoriert)
            if data.startswith(MANUAL_START_STRING):
                manual_mode = True
                logging.info('Aktiviere manuellen Modus: Rivendell-Pakete werden ignoriert.')
                continue
            elif data.startswith(MANUAL_STOP_STRING):
                manual_mode = False
                logging.info('Beende manuellen Modus: Rivendell-Pakete werden wieder angezeigt.')
                continue

            # Empfangene Zeichenkette aufteilen.
            try:
                group, artist, song, ms, nonsense = data.rstrip('\n').split(SPLIT_CHAR)
            except ValueError:
                logging.error("Ignoring packet with wrong value format: " + repr(data))
                continue

            # Zeichenketten von Leerzeichen etc. säubern.
            artist = remove_control_characters(artist.strip())
            song = remove_control_characters(song.strip())
            group = remove_control_characters(group.strip())
            ms = ms.strip()
            date = datetime.now(pytz.utc)

            # Falls der manuelle Modus aktiv ist, füge nur Elemente mit Gruppe MANUAL_GROUP in die Playlist ein.
            if manual_mode and not (group == MANUAL_GROUP):
                logging.info("Ignoriere Song im manuellen Modus: " + repr(data))
                continue

            # Nur die Events in die Playlist einpflegen, die wir oben angegeben haben.
            if group in WANTED_GROUPS:

                # ignoriere Duplikate
                if not (artist == former_artist and song == former_song):

                    former_artist = artist
                    former_song = song

                    if option.xspf:
                        logging.debug("Creating XSPF output")
                        create_xspf_track(artist, song, ms, date)

                    if option.web or option.text:
                        logging.debug("Creating WEB/TEXT output")
                        create_plain_output(artist, song, group, ms, date, option.text, option.web)

                        # TODO: run-parts  ${HIRSE_META_SCRIPT_DIR} ??
                        if not option.dev:
                            metad_run = "run-parts"

                            metad_args = [metad_run, HIRSE_META_SCRIPT_DIR]

                            p = sp.Popen(metad_args, bufsize=1)
                            p.communicate()
                else:
                    logging.debug("Ignoring duplicate " + song + "from" + artist)
            else:
                logging.debug("Excluding GROUP: _" + group + "_")

            # Variablen leeren, insbesondere group, da es für "if" genutzt wird
            song = ""
            group = ""

        except KeyboardInterrupt:
            logging.debug("\nExit listener by user interrupt.")
            clean_exit(0)

# TODO: kills erkennen und sauber beenden


if __name__ == "__main__":
    main()
